'''
Load initial pressures from BRAGFLO ics.txt.
Glenn Hammond 6/22/17
'''

import sys
import numpy as np
from h5py import *

sys.path.insert(0,'../grid')
from load_ics import load

liquid_pressure,liquid_saturation = load('../grid/ics.txt')

filename = 'initial_pressure_dataset.h5'
h5file = File(filename,'w')
# cell ids
iarray = np.arange(1,len(liquid_pressure)+1,dtype='=i4')
h5file.create_dataset('Cell Ids',data=iarray)
# pressure array
farray = np.zeros(iarray.size,dtype='=f8')
farray[:] = liquid_pressure[:]
h5file.create_dataset('soil_reference_pressure',data=farray)
h5file.close()

print('done')
