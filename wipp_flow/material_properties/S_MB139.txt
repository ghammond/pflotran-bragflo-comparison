  ID 3
  POROSITY 1.1000000d-02
  TORTUOSITY 1.0000000d+00
  ROCK_DENSITY 2.6500000d+03
  SOIL_COMPRESSIBILITY_FUNCTION BRAGFLO
  BULK_COMPRESSIBILITY 2.23d-11 #bulk
  EXTERNAL_FILE ../material_properties/soil_reference_pressure.txt
  PERMEABILITY
    PERM_X_LOG10 -18.9796 #-18.89
    PERM_Y_LOG10 -18.9796 #-18.89
    PERM_Z_LOG10 -18.9796 #-18.89
  /
  WIPP-FRACTURE
    INITIATING_PRESSURE 2.000000d+05
    ALTERED_PRESSURE 3.800000d+06
    MAXIMUM_FRACTURE_POROSITY 5.d-2
    FRACTURE_EXPONENT 1.518045d+01
    ALTER_PERM_X
    ALTER_PERM_Y
  /

