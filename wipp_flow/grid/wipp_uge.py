'''
read cell_dimensions.txt, calculate cell centers and volumes, face centers and areas,
write explicit unstructured grid (uge) that is the same as BRAGFLO 2D flaring grid
for use with PFLOTRAN

Makes a flat grid to run with adjusted gravity vector (calculates this)
Makes a grid with adjusted elevations in the Salado, using original BRAGFLO method,
which results in the same dip I calculated using tan()

Add calculation of initial conditions. Copy methods used by ALGEBRA. Create cell-indexed h5 file.
Add write of boundary regions (.ex) and boundary conditions (dirichlet)

Emily 2.17.17
'''

import sys
import os
import numpy as np
from h5py import *

from load_ics import load
 
#Turns out these are global variables - using them as such, tho not sure of goodness of technique
infilename = 'cell_dimensions.txt'
outfilename = 'wipp.uge'
outfile2name = 'wipp_dip.uge'
domainfilename = 'wipp-domain.h5'
domainfile2 = 'wipp-domain-dip.h5'
domainfile3 = 'domain-equal.h5'
domainfile4 = 'domain-dip-equal.h5'
topbcname = 'wipp_top.ex'
culbcname = 'wipp_culebra.ex'
magbcname = 'wipp_magenta.ex'
bcfilename = 'bc.txt'
gfilename = 'grid.txt'
rfilename = 'regions.txt' #includes regions for obs pts
sfilename = 'strata.txt'
ofilename = 'obspts.txt'
mfilename = 'materials.h5'
icpfilename = 'pressure_nodip.h5'
icpdfilename = 'pressure_dip.h5'
ncol = 68
nrow = 33 #in BRAGFLOW grid
dip = 1 #degree

#EARTH_GRAVITY = -9.8068 # m/s^2, copied from pflotran_constants.F90, not using this
COMPRES = 3.1e-10 #from wipp db: BRINESAL:COMPRES (CCA, version 1)
DNSFLUID = 1220.  #from wipp db: BRINESAL:DNSFLUID (CCA, version 1)
REF_PRES = 101325. #from wipp db: BRINESAL:REF_PRES (CCA, version 1)
P0 = 12197479.0626168 #sampled parameter: S_HALITE:PRESSURE
GRAVACC = 9.80665 #from wipp db: REFCON:GRAVACC (CCA, version 1)
#Reference locations defined in terms of these nodes, X, Y(elevation), Z(pflotran Y)
n2112 = [2.33920e4,9.79990e2,-1.60900e1] #for Rustler+ potential and pressure calcs
n2113 = [2.34907e4,9.79990e2,-4.75000e0]
n457 = [2.34907e4,3.78260e2,-4.75000e0] #for Salado elevation, potential, and pressure calcs
n458 = [2.35007e4,3.78260e2,-2.58350e1]
n526 = [2.34907e4,3.79110e2,-4.75000e0]

gas_sat1 = 1. #CAVITY_1 - _5 are dry to start, I'm approximating
gas_sat4 = 1. - 0.083630 #SANTAROS and top of DEWYLAKE
brine_pres = 13312200. #liquid pressure in Castile brine reservoir
culebra_pres = 933300. #liquid pressure in the Culebra
magenta_pres = 963100. #liquid pressure in the Magenta
temperature = 27. #the temperature Heeho was using in prepflotran
gassat_zero = 0. #for use with forced two phase

grid_template = \
'''
GRID
  TYPE UNSTRUCTURED_EXPLICIT ../../grid/%s
  GRAVITY %f %f %f
END
'''
reg_template = \
'''
REGION %s
  FILE ../../grid/%s.txt
/
'''
sinit_template = \
'''
STRATA
  MATERIAL ../../grid/%s
/
'''
schange_template = \
'''
STRATA
  REGION %s
  MATERIAL %s
  START_TIME %f y
  FINAL_TIME %f y
/
'''
obs_template = \
'''
OBSERVATION
  REGION %s
/
'''
flow1_template = \
'''
FLOW_CONDITION %s
  TYPE
    LIQUID_PRESSURE DIRICHLET
    MOLE_FRACTION DIRICHLET
    TEMPERATURE DIRICHLET
  /
  LIQUID_PRESSURE %f
  MOLE_FRACTION %f
  TEMPERATURE %f
END
'''
flow3_template = \
'''
FLOW_CONDITION %s
  TYPE
    GAS_PRESSURE DIRICHLET
    GAS_SATURATION DIRICHLET
    TEMPERATURE DIRICHLET
  /
  GAS_PRESSURE %f
  GAS_SATURATION %f
  TEMPERATURE %f
END
'''

#Need to open and close these files before and after using fns
def grid_to_file(fp,ugename,gravity):
  fp.write(grid_template % (ugename,gravity[0],gravity[1],gravity[2]))
  return
def reg_to_file(fp,rname):
  fp.write(reg_template % (rname,rname))
  return
def obs_to_file(fp,rname):
  fp.write(obs_template % (rname))
  return
def sinit_to_file(fp,mfilename):
  fp.write(sinit_template % mfilename)
  return
def schange_to_file(fp,rname,mname,start,end):
  fp.write(schange_template % (rname,mname,start,end))
  return
def flow1_to_file(fp,fcname,lp,mf,t):
  fp.write(flow1_template % (fcname,lp,mf,t))
  return
def flow3_to_file(fp,fcname,gp,gs,t):
  fp.write(flow3_template % (fcname,gp,gs,t))
  return

def read_dimensions():
  xwidth = []
  ywidth = []
  zwidth = []
  f = open(infilename,'r')
  while(True):
    line = f.readline()
    if line.startswith('XY'):
      while(True):
        line = f.readline()
        try:
          x = float(line.split()[0])
          #print('x = %f' % x)
          y = float(line.split()[1])
          #print('y = %f' % y)
          xwidth.append(x)
          ywidth.append(y)
        except ValueError:
          print('Done reading XY, Z next')
          break
      if line.startswith('Z'): #indented so that this only happens if XY is done
        while(True):
          line = f.readline()
          try:
            z = float(line.split()[0])
            #print('z = %f' % z)
            zwidth.append(z)
          except: #I don't know what kind of error this is, it is not a ValueError
            print('Done reading Z')
            break
        break

  f.close()
  print('Sum x = %f' % sum(xwidth))
  print('Height of repository = %f' % sum(zwidth[6:17]))
  print('Base of repository = %f' % sum(zwidth[0:6]))
  print('Repository to surface = %f' % sum(zwidth[17:33]))
  return([xwidth,ywidth,zwidth])

def planview_areas(dimensions):
  '''Calculate plan view areas (xy in my mind, xz in wipp world),
     so I can make 3d grid with similar cell volumes.
     1.16.17'''
  xwidth = dimensions[0]
  ywidth = dimensions[1]
  f = open('planview_areas.txt','w')
  areas = []
  for i in range(len(xwidth)):
    areas.append(xwidth[i]*ywidth[i])
    f.write('%f\n' % (areas[i]))
  f.close()
  print('planview_areas.txt written')
  return areas

def adjust_gravity(dip): #input dip in degrees
  #EARTH_GRAVITY = -9.8068 # m/s^2, copied from pflotran_constants.F90
  gravity = -GRAVACC #use WIPP value
  z_component = np.cos(np.radians(dip))*gravity
  x_component = np.sin(np.radians(dip))*gravity
  adjgravity = [x_component,0.0,z_component]
  print('Adjusted gravity vector %f %f %f' % (x_component, 0.0, z_component))
  return adjgravity
  
def wipp_uge(dimensions):
  xwidth = dimensions[0]
  ywidth = dimensions[1]
  zwidth = dimensions[2]
  ncell = ncol*nrow
  f = open(outfilename,'w')
  f2 = open(outfile2name,'w')
  f3 = open(topbcname,'w')
  f4 = open(culbcname,'w')
  f5 = open(magbcname,'w')

  ycenter = 0.
  xcenter = []
  zcenter = []
  vol = []
  xarea = []
  zarea = []
  area_left = [] #save only for first column
  area_right = []
  area_up = []
  vertices = []
  icp = [] #initial pressures with no dip in Salado
  #variables below are used to make second uge file with dip_adjusted Z in the Salado
  #disconnect Salado from Castile and from overlying units except at shaft and borehole
  x0 = (n457[0]+n458[0])/2.
  z0 = (n457[1]+n526[1])/2.
  zc_dip = [] #for corrected cell centers
  zu_dip = [] #for corrected up connections
  zr_dip = [] #for corrected right connections
  icp_dip = [] #init P with dip in Salado
  #calculate centers, vol, area, and vertices!
  #also calculate initial P(liquid) for whole domain assuming saturation, will overwrite in input deck as necessary
  den0 = DNSFLUID*np.exp(COMPRES*(P0-REF_PRES))
  phiref = (1./DNSFLUID - 1./den0)/(GRAVACC*COMPRES)
  print('den0 = %f and phiref = %f' %(den0,phiref))
  phiref2 = 0. #for use in beds above the Salado
  x02 = (n2112[0]+n2113[0])/2.
  z02 = n2112[1]
  z1 = 0.
  for j in range(nrow):
    x1 = 0.
    z2 = z1 + zwidth[j]
    for i in range(ncol):
      x2 = x1 + xwidth[i]
      xcenter.append((x1+x2)/2.)
      vol.append(xwidth[i]*ywidth[i]*zwidth[j])
      xarea.append(x2)
      zcenter.append((z1+z2)/2.)
      zarea.append(z2)
      den = -1./(GRAVACC*COMPRES*(phiref - (zcenter[-1]-z0) - 1./(GRAVACC*DNSFLUID*COMPRES)))
      icp.append(1./COMPRES * np.log(den/DNSFLUID) + REF_PRES)
      if icp[-1] < REF_PRES:
        icp[-1] = REF_PRES #this makes pressures in the dewylake and santarosa atm., which doesn't matter because I reset them later anyway
    #Make a version with dipping Salado, include weird icp in Rustler, Dewey Lake, and Santa Rosa
      if (j>=2 and j<24): #if in the Salado
        zc_dip.append(np.cos(np.radians(dip))*(zcenter[-1] - z0) + np.sin(np.radians(dip))*(xcenter[-1]-x0) + z0)
        zu_dip.append(np.cos(np.radians(dip))*(zarea[-1] - z0) + np.sin(np.radians(dip))*(xcenter[-1]-x0) + z0) #above xcenter of cell
        zr_dip.append(np.cos(np.radians(dip))*(zcenter[-1] - z0) + np.sin(np.radians(dip))*(xarea[-1]-x0) + z0) #between xcenters
      else: #otherwise it is the same as original
        zc_dip.append(zcenter[-1])
        zu_dip.append(zarea[-1])
        zr_dip.append(zcenter[-1])
      if (j>=24): #if above the Salado, reference pressure and potential are this:
        den = -1./(GRAVACC*COMPRES*(phiref2 - (zc_dip[-1]-z02) - 1./(GRAVACC*DNSFLUID*COMPRES)))
        icp_dip.append(1./COMPRES * np.log(den/DNSFLUID) + REF_PRES)
      else: #ref pressure etc defined by farfield pressure in the Salado
        den = -1./(GRAVACC*COMPRES*(phiref - (zc_dip[-1]-z0) - 1./(GRAVACC*DNSFLUID*COMPRES)))
        icp_dip.append(1./COMPRES * np.log(den/DNSFLUID) + REF_PRES)
        #print(i,j,icp_dip[-1])
      if icp_dip[-1] < REF_PRES: #makes pressures in the dewylake and santarosa atm., which doesn't matter because I reset them later anyway
        icp_dip[-1] = REF_PRES
    #And continue with stuff that happens with or without dip
      if i == 0:
        area_left.append(ywidth[i]*zwidth[j]) #only for left faces
      if i == ncol-1:
        area_right.append(ywidth[i]*zwidth[j]) #tho there is not a right connection
      else:
        area_right.append(min((ywidth[i]*zwidth[j]),(ywidth[i+1]*zwidth[j])))
      area_up.append(xwidth[i]*ywidth[i])
      y2 = ywidth[i]/2.
      y1 = -y2
      vertices.extend([[x1,y1,z1],[x2,y1,z1],[x2,y2,z1],[x1,y2,z1]]) #these are listed in order!
      x1 += xwidth[i] #update for next iter
    z1 += zwidth[j]  #update for next iter
  #save last set of vertices
  x1 = 0. #z1 is correct after last iter of previous loop
  for i in range(ncol):
    x2 = x1+xwidth[i] #x and y need to be recalc'd
    y2 = ywidth[i]/2.
    y1 = -y2
    vertices.extend([[x1,y1,z1],[x2,y1,z1],[x2,y2,z1],[x1,y2,z1]]) #these are listed in order!
    x1 += xwidth[i]
  print('top of domain is at z = %f m' % z2)
  print('end of domain is at z = %f m' % x2)
  zmax = z2 #save for use in top bc
  xmax = x2 #save for use in right bc
  #save centers XC,YC,ZC for h5 file
  xarray = np.array(xcenter,dtype=np.float64)
  yarray = np.zeros(len(xcenter),dtype=np.float64)
  zarray = np.array(zcenter,dtype=np.float64)
  zarray_dip = np.array(zc_dip,dtype=np.float64)
  #save IC pressures for IC h5 file
  icparray = np.array(icp,dtype=np.float64)
  icparray_dip = np.array(icp_dip,dtype=np.float64)
  #write cells for uges with and without dip
  f.write('CELLS %i\n' % ncell)
  f2.write('CELLS %i\n' % ncell)
  for j in range(nrow):
    for i in range(ncol):
      index = i+(j*ncol)
      cellid = index+1
      f.write('%i %f %f %f %f\n' % (cellid,xcenter[index],ycenter,zcenter[index],vol[index]))
      f2.write('%i %f %f %f %f\n' % (cellid,xcenter[index],ycenter,zc_dip[index],vol[index]))
  #write connections for uges with and without dip
  ncon = (ncol-1)*nrow + ncol*(nrow-1)
  f.write('CONNECTIONS %i\n' % ncon)
  f2.write('CONNECTIONS %i\n' % (ncon-2*ncol+3)) #cons only at bh (2 layers) and shaft (1 layer)
  f3.write('CONNECTIONS %i\n' % (ncol-1))
  f4.write('CONNECTIONS %i\n' % 2) #only the Culebra and the Magenta
  f5.write('CONNECTIONS %i\n' % 2) #only the Culebra and the Magenta
  for j in range(nrow):
    for i in range(ncol):
      index = i+(j*ncol)
      cellid = index+1
      if i < ncol-1:
        right = cellid + 1
        f.write('%i %i %f %f %f %f\n' % (cellid,right,xarea[index],ycenter,zcenter[index],area_right[index]))
        f2.write('%i %i %f %f %f %f\n' % (cellid,right,xarea[index],ycenter,zr_dip[index],area_right[index]))
      if i == 0 and j == 25: #culebra left
        f4.write('%i %f %f %f %f\n' % (cellid,0.,ycenter,zcenter[index],area_left[j]))
      if i == ncol-1 and j == 25: #culebra right
        f4.write('%i %f %f %f %f\n' % (cellid,xmax,ycenter,zcenter[index],area_right[index]))
      if (i == 0 and j ==27): #magenta left
        f5.write('%i %f %f %f %f\n' % (cellid,0.,ycenter,zcenter[index],area_left[j]))
      if (i == ncol-1 and j ==27): #magenta right
        f5.write('%i %f %f %f %f\n' % (cellid,xmax,ycenter,zcenter[index],area_right[index]))
      if j < nrow-1:
        up = cellid + ncol
        f.write('%i %i %f %f %f %f\n' % (cellid,up,xcenter[index],ycenter,zarea[index],area_up[index]))
        if (j!=1 and j!=23): #if not between castile and salado or salado and above, maintain connection
          f2.write('%i %i %f %f %f %f\n' % (cellid,up,xcenter[index],ycenter,zu_dip[index],area_up[index]))
        elif (j==23 and i==42): #also maintain connection at shaft
          print('j==23 and i==42, %i, %i' %(j,i))
          f2.write('%i %i %f %f %f %f\n' % (cellid,up,xcenter[index],ycenter,zu_dip[index],area_up[index]))
        elif (j==1 and i==25 or j==23 and i==25): #shift connection at bh is not necessary
          print('j==1 and i==25 or j==23 and i==25, %i, %i' % (j,i))
          f2.write('%i %i %f %f %f %f\n' % (cellid,up,xcenter[index],ycenter,zu_dip[index],area_up[index]))
      if j == nrow-1: #top boundary
        if i != 42: #shaft isn't included in the bc
          f3.write('%i %f %f %f %f\n' % (cellid,xcenter[index],ycenter,zmax,area_up[index]))
#        else:
#          f3.write('%i %f %f %f %f\n' % (cellid,xcenter[index],ycenter,zmax,area_up[index]))

  #write vertices and store for h5 file (only for no-dip grid)
  nver = len(vertices)
  f.write('VERTICES %i\n' % nver)
  for j in range(nver):
    f.write('%f %f %f\n' % (vertices[j][0],vertices[j][1],vertices[j][2]))
  varray = np.array(vertices,dtype=np.float64)
  #write elements (only for no-dip grid)
  nel = ncell
  earray = np.zeros(nel*9,'=i4')
  f.write('ELEMENTS %i\n' % nel) #don't know if PFLOTRAN does anything with this, but xdmf uses
  for j in range(nrow):          #0-indexed vertices to define elements/cells
    for i in range(ncol):
      iel = i*9+j*ncol*9 
      index = i*4+(j*ncol*4)
      index2 = i*4+(j+1)*ncol*4 #one row up
      f.write('H %i %i %i %i %i %i %i %i\n' % (index,index+1,index+2,index+3,
                                               index2,index2+1,index2+2,index2+3))
      this_el = np.array([9,index,index+1,index+2,index+3,index2,index2+1,index2+2,index2+3],dtype=np.float64)
      earray[iel:iel+9] = this_el
  f.close()
  f2.close()
  f3.close()
  print('wipp.uge written')
  print('wipp_dip.uge written')
  #write elements/cells and vertices to h5 file for paraview
  iarray = np.zeros((ncell),'i4')
  for i in range(ncell):
    iarray[i] = i+1
  h5f = File(domainfilename,'w')
  h5f.create_dataset('Domain/Cells', data=earray)
  h5f.create_dataset('Domain/Vertices',data=varray)
  h5f.create_dataset('Domain/XC',data=xarray)
  h5f.create_dataset('Domain/YC',data=yarray)
  h5f.create_dataset('Domain/ZC',data=zarray)
  h5f.create_dataset('Domain/Cell_Ids',data=iarray)
  h5f.close()
  #and write them again with zc adjusted for dip, but vertices not adjusted
  h5f = File(domainfile2,'w')
  h5f.create_dataset('Domain/Cells', data=earray)
  h5f.create_dataset('Domain/Vertices',data=varray)
  h5f.create_dataset('Domain/XC',data=xarray)
  h5f.create_dataset('Domain/YC',data=yarray)
  h5f.create_dataset('Domain/ZC',data=zarray_dip)
  h5f.create_dataset('Domain/Cell_Ids',data=iarray)
  h5f.close()
  #second domain for each with equally spaced vertices,just for viz
  #observe pflotran_cell_numbering_schemes.pdf
  varray = np.zeros((2*(ncol+1)*(nrow+1),3),'f8')
  for k in range(2):
    for j in range(nrow+1):
      for i in range(ncol+1):
        index = i + j*(ncol+1) + k*(ncol+1)*(nrow+1)
        varray[index] = [float(i),float(k)-0.5,float(j)]
  earray = np.zeros(ncol*nrow*9,'i4')
  for j in range(nrow):
    for i in range(ncol):
      iel = i*9+j*ncol*9
      n1 = i+j*(ncol+1)
      n4 = (ncol+1)*(nrow+1) + n1
      n5 = (i+(j+1)*(ncol+1))
      n8 = (ncol+1)*(nrow+1) + n5
      this_el = np.array([9,n1,n1+1,n4+1,n4,n5,n5+1,n8+1,n8])
      earray[iel:iel+9] = this_el
  #no dip, equally spaced vertices
  h5f = File(domainfile3,'w')
  h5f.create_dataset('Domain/Cells', data=earray)
  h5f.create_dataset('Domain/Vertices',data=varray)
  h5f.create_dataset('Domain/XC',data=xarray)
  h5f.create_dataset('Domain/YC',data=yarray)
  h5f.create_dataset('Domain/ZC',data=zarray)
  h5f.create_dataset('Domain/Cell_Ids',data=iarray)
  h5f.close()
  #zc adjusted for dip, equally spaced vertices
  h5f = File(domainfile4,'w')
  h5f.create_dataset('Domain/Cells', data=earray)
  h5f.create_dataset('Domain/Vertices',data=varray)
  h5f.create_dataset('Domain/XC',data=xarray)
  h5f.create_dataset('Domain/YC',data=yarray)
  h5f.create_dataset('Domain/ZC',data=zarray_dip)
  h5f.create_dataset('Domain/Cell_Ids',data=iarray)
  h5f.close()
  #IC pressures, no dip, tho probably should use w/ dip values anyway because of g adjusted
  h5f = File(icpfilename,'w')
  h5f.create_dataset('Cell_Ids',data=iarray)
  h5f.create_dataset('Pressure',data=icparray)
  h5f.close()
  #IC pressures, with dip
  h5f = File(icpdfilename,'w')
  h5f.create_dataset('Cell_Ids',data=iarray)
  h5f.create_dataset('Pressure',data=icparray_dip)
  h5f.close()
  #IC pressures with dip for checkpoint file
  icp_chpt = np.zeros((len(icparray_dip)*2),'=f8')
  counter = 0
  for i in range(len(icparray_dip)):
    icp_chpt[counter] = icparray_dip[i]
    icp_chpt[counter+1] = gassat_zero
    #icp_chpt[counter+2] = temperature
    #counter += 3
    counter += 2
  print(len(icparray_dip),len(icp_chpt),counter)

  return icp_chpt

def wipp_mat():
  #I copied this list from Brad's pfd_mat.mat, some day maybe read it in directly
  mat_list = ['S_HALITE','DRZ_0','S_MB139','S_ANH_AB','S_MB138','CAVITY_1','CAVITY_2','CAVITY_3',
    'CAVITY_4','IMPERM_Z','CASTILER','DRZ_OE_0','DRZ_PC_0','CAVITY_5','OPS_AREA','EXP_AREA',
    'CULEBRA','MAGENTA','DEWYLAKE','SANTAROS','WAS_AREA','DRZ_1','DRZ_PC_1','PCS_T1',
    'CONC_PLG','BH_OPEN','BH_SAND','BH_CREEP','UNNAMED','TAMARISK','FORTYNIN','REPOSIT',
    'CONC_MON','SHFTU','SHFTL_T1','SHFTL_T2','DRZ_OE_1','PCS_T2','PCS_T3','DRZ_PCS',]
  mat_dict = {'INACTIVE':0} #assign material id numbers
  i = 1
  for mat in mat_list:
    mat_dict[mat] = i
    i += 1
  iarray = np.zeros((ncol*nrow),'=i4')
  marray = np.zeros((ncol*nrow),'=i4')
  #locations of material changes
  iwp1 = 22
  ibh = 25
  iwp2 = 26
  ipcs1 = 29
  isror = 31
  ipcs2 = 33
  inror = 35
  ipcs3 = 37
  iops = 39
  ishaft = 42
  iexp = 43
  iout = 45
  #times of material changes, map numbers refer to Brad's color-coded material maps in xl file
  #map1 = 0. #subtract 5 from all of these, when -5 start is implemented
  map2 = 5.
  map3 = 105.
  map4 = 205.
  map5 = 355.
  map6 = 555.
  map7 = 1555.
  finalt = 10005.
  #regions needed for material changes and/or initial conditions
  #maybe make this a dictionary of names, so can loop through writing files and region blocks
  reg_dict = {
       'rDRZ' : [],
       'rDRZ_PCS' : [],
       'rDRZ_OE' : [],
       'rWAS_AREA' : [],
       'rREPOSIT' : [],
       'rSROR' : [], #portion of rREPOSIT required for pressure output
       'rNROR' : [], #portion of rREPOSIT required for pressure output
       'rPCS' : [],
       'rSPCS' : [], #south panel closure required for pressure output
       'rMPCS' : [], #middle panel closure, ditto
       'rNPCS' : [], #north panel closure, ditto
       'rOPS_AREA' : [],
       'rEXP_AREA' : [],
       'rCONC_MON' : [],
       'rSHFTL' : [],
       'rSHFTU' : [],
       'rCONC_PLG' : [], #borehole plug, this will go to BH_SAND
       'rBH_OPEN' : [], #borehole not plugged, this will go to BH_SAND
       'rBH_CREEP' : [], #lower part of borehole, this will go to BH_SAND then BH_CREEP
       'rUNNAMED' : [],
       'rCULEBRA' : [],
       'rMAGENTA' : [],
       'rDEWYLAKE' : [],
       'rSANTAROS' : [],
       'rTAMARISK' : [],
       'rFORTYNIN' : [],
       'rS_HALITE' : [],
       'rMB' : [], #cheat because ANH_AB, MB138, and MB139 are all given the same properties
       'rCASTILE' : [],
       'rCASTILER' : [],
       'rUNSAT' : [] #top of Dewey Lake and all of Santa Rosa is unsaturated
  }
  for j in range(nrow):
    for i in range(ncol):
      index = i+(j*ncol)
      iarray[index] = index+1 #cell ids
      if (j < 1):
        if (i<iwp1): #cells 1-22
          marray[index] = mat_dict.get('IMPERM_Z') #Castile
          reg_dict['rCASTILE'].append(index+1)
        elif (i<ibh): #cell 23-25
          marray[index] = mat_dict.get('CASTILER') #Castile Brine Reservoir (CASTILER)
          reg_dict['rCASTILER'].append(index+1)
        elif (i<iwp2): #cell 26
          marray[index] = mat_dict.get('CASTILER') #Lower Borehole (BH_CREEP)
          reg_dict['rBH_OPEN'].append(index+1)
          reg_dict['rBH_CREEP'].append(index+1)
          reg_dict['rCASTILER'].append(index+1)
        elif (i<iout): #cell 27-45
          marray[index] = mat_dict.get('CASTILER') #Castile Brine Reservoir (CASTILER)
          reg_dict['rCASTILER'].append(index+1)
        elif (i<ncol): #to end of row 1
          marray[index] = mat_dict.get('IMPERM_Z') #Castile
          reg_dict['rCASTILE'].append(index+1)
      elif (j < 2):
        if (i<ibh):
          marray[index] = mat_dict.get('IMPERM_Z') #Castile
          reg_dict['rCASTILE'].append(index+1)
        elif (i<iwp2):
          marray[index] = mat_dict.get('IMPERM_Z') #BH_CREEP
          reg_dict['rCASTILE'].append(index+1)
          reg_dict['rBH_OPEN'].append(index+1)
          reg_dict['rBH_CREEP'].append(index+1)
        elif (i<ncol):
          marray[index] = mat_dict.get('IMPERM_Z') #Castile
          reg_dict['rCASTILE'].append(index+1)
      elif (j < 6):
        if (i<ibh):
          marray[index] = mat_dict.get('S_HALITE') #1 #Salado (S_HALITE)
          reg_dict['rS_HALITE'].append(index+1)
        elif (i<iwp2):
          marray[index] = mat_dict.get('S_HALITE') #BH_CREEP
          reg_dict['rBH_OPEN'].append(index+1)
          reg_dict['rBH_CREEP'].append(index+1)
        elif (i<ncol):
          marray[index] = mat_dict.get('S_HALITE') #Salado
          reg_dict['rS_HALITE'].append(index+1)
      elif (j < 7):
        if (i<iwp1):
          marray[index] = mat_dict.get('S_MB139') #MB139
          reg_dict['rMB'].append(index+1)
        elif (i<ibh):
          marray[index] = mat_dict.get('DRZ_0') #DRZ
          reg_dict['rDRZ'].append(index+1)
        elif (i<iwp2):
          marray[index] = mat_dict.get('DRZ_0') #BH_CREEP
          reg_dict['rDRZ'].append(index+1)
          reg_dict['rBH_CREEP'].append(index+1)
        elif (i<ipcs1):
          marray[index] = mat_dict.get('DRZ_0') #DRZ
          reg_dict['rDRZ'].append(index+1)
        elif (i<isror):
          marray[index] = mat_dict.get('S_MB139') #MB139
          reg_dict['rMB'].append(index+1)
        elif (i<ipcs2):
          marray[index] = mat_dict.get('DRZ_0') #DRZ
          reg_dict['rDRZ'].append(index+1)
        elif (i<inror):
          marray[index] = mat_dict.get('S_MB139') #MB139
          reg_dict['rMB'].append(index+1)
        elif (i<ipcs3):
          marray[index] = mat_dict.get('DRZ_0') #DRZ
          reg_dict['rDRZ'].append(index+1)
        elif (i<iops):
          marray[index] = mat_dict.get('S_MB139') #MB139
          reg_dict['rMB'].append(index+1)
        elif (i<ishaft):
          marray[index] = mat_dict.get('DRZ_OE_0') #DRZ
          reg_dict['rDRZ_OE'].append(index+1)
        elif (i<iexp):
          marray[index] = mat_dict.get('S_MB139') #MB139
          reg_dict['rMB'].append(index+1)
        elif (i<iout):
          marray[index] = mat_dict.get('DRZ_OE_0') #DRZ
          reg_dict['rDRZ_OE'].append(index+1)
        elif (i<ncol):
          marray[index] = mat_dict.get('S_MB139') #MB139
          reg_dict['rMB'].append(index+1)
      elif (j < 9):
        if (i<iwp1):
          marray[index] = mat_dict.get('S_HALITE') #1 #Salado
          reg_dict['rS_HALITE'].append(index+1)
        elif (i<ibh):
          marray[index] = mat_dict.get('DRZ_0') #DRZ
          reg_dict['rDRZ'].append(index+1)
        elif (i<iwp2):
          marray[index] = mat_dict.get('DRZ_0') #BH_CREEP
          reg_dict['rDRZ'].append(index+1)
          reg_dict['rBH_CREEP'].append(index+1)
        elif (i<ipcs1):
          marray[index] = mat_dict.get('DRZ_0') #DRZ
          reg_dict['rDRZ'].append(index+1)
        elif (i<isror):
          marray[index] = mat_dict.get('DRZ_PC_0') #DRZ_PCS
          reg_dict['rDRZ_PCS'].append(index+1)
        elif (i<ipcs2):
          marray[index] = mat_dict.get('DRZ_0') #DRZ
          reg_dict['rDRZ'].append(index+1)
        elif (i<inror):
          marray[index] = mat_dict.get('DRZ_PC_0') #DRZ_PCS
          reg_dict['rDRZ_PCS'].append(index+1)
        elif (i<ipcs3):
          marray[index] = mat_dict.get('DRZ_0') #DRZ
          reg_dict['rDRZ'].append(index+1)
        elif (i<iops):
          marray[index] = mat_dict.get('DRZ_PC_0') #DRZ_PCS
          reg_dict['rDRZ_PCS'].append(index+1)
        elif (i<ishaft):
          marray[index] = mat_dict.get('DRZ_OE_0')  #DRZ
          reg_dict['rDRZ_OE'].append(index+1)
        elif (i<iexp):
          marray[index] = mat_dict.get('CAVITY_4') #Concrete Monolith
          reg_dict['rCONC_MON'].append(index+1)
        elif (i<iout):
          marray[index] = mat_dict.get('DRZ_OE_0') #DRZ
          reg_dict['rDRZ_OE'].append(index+1)
        elif (i<ncol):
          marray[index] = mat_dict.get('S_HALITE') #Salado
          reg_dict['rS_HALITE'].append(index+1)
      elif (j < 12):
        if (i<iwp1):
          marray[index] = mat_dict.get('S_HALITE') #Salado
          reg_dict['rS_HALITE'].append(index+1)
        elif (i<ibh):
          marray[index] = mat_dict.get('CAVITY_1') #Waste Panel
          reg_dict['rWAS_AREA'].append(index+1)
        elif (i<iwp2):
          marray[index] = mat_dict.get('CAVITY_1') #Upper Borehole (BH_SAND)
          reg_dict['rWAS_AREA'].append(index+1)
          reg_dict['rBH_OPEN'].append(index+1)
        elif (i<ipcs1):
          marray[index] = mat_dict.get('CAVITY_1') #Waste Panel
          reg_dict['rWAS_AREA'].append(index+1)
        elif (i<isror):
          marray[index] = mat_dict.get('CAVITY_5') #ROMPCS
          reg_dict['rPCS'].append(index+1)
          reg_dict['rSPCS'].append(index+1)
        elif (i<ipcs2):
          marray[index] = mat_dict.get('CAVITY_2') #SRoR (REPOSIT)
          reg_dict['rREPOSIT'].append(index+1)
          reg_dict['rSROR'].append(index+1)
        elif (i<inror):
          marray[index] = mat_dict.get('CAVITY_5') #ROMPCS
          reg_dict['rPCS'].append(index+1)
          reg_dict['rMPCS'].append(index+1)
        elif (i<ipcs3):
          marray[index] = mat_dict.get('CAVITY_2') #NRoR
          reg_dict['rREPOSIT'].append(index+1)
          reg_dict['rNROR'].append(index+1)
        elif (i<iops):
          marray[index] = mat_dict.get('CAVITY_5') #ROMPCS
          reg_dict['rPCS'].append(index+1)
          reg_dict['rNPCS'].append(index+1)
        elif (i<ishaft):
          marray[index] = mat_dict.get('CAVITY_3') #OPS
          reg_dict['rOPS_AREA'].append(index+1)
        elif (i<iexp):
          marray[index] = mat_dict.get('CAVITY_4') #Concrete Monolith
          reg_dict['rCONC_MON'].append(index+1)
        elif (i<iout):
          marray[index] = mat_dict.get('CAVITY_3') #EXP
          reg_dict['rEXP_AREA'].append(index+1)
        elif (i<ncol):
          marray[index] = mat_dict.get('S_HALITE') #Salado
          reg_dict['rS_HALITE'].append(index+1)
      elif (j < 13):
        if (i<iwp1):
          marray[index] = mat_dict.get('S_HALITE') #Salado
          reg_dict['rS_HALITE'].append(index+1)
        elif (i<ibh):
          marray[index] = mat_dict.get('DRZ_0') #DRZ
          reg_dict['rDRZ'].append(index+1)
        elif (i<iwp2):
          marray[index] = mat_dict.get('DRZ_0') #Upper Borehole
          reg_dict['rDRZ'].append(index+1)
          reg_dict['rBH_OPEN'].append(index+1)
        elif (i<ipcs1):
          marray[index] = mat_dict.get('DRZ_0') #DRZ
          reg_dict['rDRZ'].append(index+1)
        elif (i<isror):
          marray[index] = mat_dict.get('DRZ_PC_0') #DRZPCS
          reg_dict['rDRZ_PCS'].append(index+1)
        elif (i<ipcs2):
          marray[index] = mat_dict.get('DRZ_0')  #DRZ
          reg_dict['rDRZ'].append(index+1)
        elif (i<inror):
          marray[index] = mat_dict.get('DRZ_PC_0')  #DRZPCS
          reg_dict['rDRZ_PCS'].append(index+1)
        elif (i<ipcs3):
          marray[index] = mat_dict.get('DRZ_0') #DRZ
          reg_dict['rDRZ'].append(index+1)
        elif (i<iops):
          marray[index] = mat_dict.get('DRZ_PC_0') #DRZPCS
          reg_dict['rDRZ_PCS'].append(index+1)
        elif (i<ishaft):
          marray[index] = mat_dict.get('DRZ_OE_0') #DRZ_OE
          reg_dict['rDRZ_OE'].append(index+1)
        elif (i<iexp):
          marray[index] = mat_dict.get('CAVITY_4') #Concrete Monolith
          reg_dict['rCONC_MON'].append(index+1)
        elif (i<iout):
          marray[index] = mat_dict.get('DRZ_OE_0') #DRZ_OE
          reg_dict['rDRZ_OE'].append(index+1)
        elif (i<ncol):
          marray[index] = mat_dict.get('S_HALITE') #Salado
          reg_dict['rS_HALITE'].append(index+1)
      elif (j < 14): 
        if (i<iwp1):
          marray[index] = mat_dict.get('S_ANH_AB') #Anhydrite AB
          reg_dict['rMB'].append(index+1)
        elif (i<ibh):
          marray[index] = mat_dict.get('DRZ_0') #DRZ
          reg_dict['rDRZ'].append(index+1)
        elif (i<iwp2):
          marray[index] = mat_dict.get('DRZ_0') #Upper Borehole
          reg_dict['rDRZ'].append(index+1)
          reg_dict['rBH_OPEN'].append(index+1)
        elif (i<ipcs1):
          marray[index] = mat_dict.get('DRZ_0')  #DRZ
          reg_dict['rDRZ'].append(index+1)
        elif (i<isror):
          marray[index] = mat_dict.get('S_ANH_AB') #Anhydrite AB
          reg_dict['rMB'].append(index+1)
        elif (i<ipcs2):
          marray[index] = mat_dict.get('DRZ_0')  #DRZ
          reg_dict['rDRZ'].append(index+1)
        elif (i<inror):
          marray[index] = mat_dict.get('S_ANH_AB') #Anhydrite AB
          reg_dict['rMB'].append(index+1)
        elif (i<ipcs3):
          marray[index] = mat_dict.get('DRZ_0') #DRZ
          reg_dict['rDRZ'].append(index+1)
        elif (i<iops):
          marray[index] = mat_dict.get('S_ANH_AB') #Anhydrite AB
          reg_dict['rMB'].append(index+1)
        elif (i<ishaft):
          marray[index] = mat_dict.get('DRZ_OE_0') #DRZ
          reg_dict['rDRZ_OE'].append(index+1)
        elif (i<iexp):
          marray[index] = mat_dict.get('S_ANH_AB') #Anhydrite AB
          reg_dict['rMB'].append(index+1)
        elif (i<iout):
          marray[index] = mat_dict.get('DRZ_OE_0') #DRZ
          reg_dict['rDRZ_OE'].append(index+1)
        elif (i<ncol):
          marray[index] = mat_dict.get('S_ANH_AB') #Anhydrite AB
          reg_dict['rMB'].append(index+1)
      elif (j < 16):
        if (i<iwp1):
          marray[index] = mat_dict.get('S_HALITE') #Salado
          reg_dict['rS_HALITE'].append(index+1)
        elif (i<ibh):
          marray[index] = mat_dict.get('DRZ_0') #DRZ
          reg_dict['rDRZ'].append(index+1)
        elif (i<iwp2):
          marray[index] = mat_dict.get('DRZ_0') #Upper Borehole
          reg_dict['rDRZ'].append(index+1)
          reg_dict['rBH_OPEN'].append(index+1)
        elif (i<ipcs1):
          marray[index] = mat_dict.get('DRZ_0') #DRZ
          reg_dict['rDRZ'].append(index+1)
        elif (i<isror):
          marray[index] = mat_dict.get('DRZ_PC_0') #DRZPCS
          reg_dict['rDRZ_PCS'].append(index+1)
        elif (i<ipcs2):
          marray[index] = mat_dict.get('DRZ_0') #DRZ
          reg_dict['rDRZ'].append(index+1)
        elif (i<inror):
          marray[index] = mat_dict.get('DRZ_PC_0') #DRZPCS
          reg_dict['rDRZ_PCS'].append(index+1)
        elif (i<ipcs3):
          marray[index] = mat_dict.get('DRZ_0') #DRZ
          reg_dict['rDRZ'].append(index+1)
        elif (i<iops):
          marray[index] = mat_dict.get('DRZ_PC_0') #DRZPCS
          reg_dict['rDRZ_PCS'].append(index+1)
        elif (i<ishaft):
          marray[index] = mat_dict.get('DRZ_OE_0') #DRZ_OE
          reg_dict['rDRZ_OE'].append(index+1)
        elif (i<iexp):
          marray[index] = mat_dict.get('CAVITY_4') #Lower Shaft (SHFTL_T1)
          reg_dict['rSHFTL'].append(index+1)
        elif (i<iout):
          marray[index] = mat_dict.get('DRZ_OE_0') #DRZ_OE
          reg_dict['rDRZ_OE'].append(index+1)
        elif (i<ncol):
          marray[index] = mat_dict.get('S_HALITE') #Salado
          reg_dict['rS_HALITE'].append(index+1)
      elif (j < 17):
        if (i<ibh):
          marray[index] = mat_dict.get('S_MB138') #MB138
          reg_dict['rMB'].append(index+1)
        elif (i<iwp2):
          marray[index] = mat_dict.get('S_MB138') #Upper Borehole
          reg_dict['rBH_OPEN'].append(index+1)
        elif (i<ncol):
          marray[index] = mat_dict.get('S_MB138') #MB138
          reg_dict['rMB'].append(index+1)
      elif (j < 24):
        if (i<ibh):
          marray[index] = mat_dict.get('S_HALITE') #Salado
          reg_dict['rS_HALITE'].append(index+1)
        elif (i<iwp2):
          marray[index] = mat_dict.get('S_HALITE')  #Upper Borehole
          reg_dict['rS_HALITE'].append(index+1)
          reg_dict['rBH_OPEN'].append(index+1)
        elif (i<ishaft):
          marray[index] = mat_dict.get('S_HALITE') #Salado
          reg_dict['rS_HALITE'].append(index+1)
        elif (i<iexp):
          marray[index] = mat_dict.get('CAVITY_4') #Lower Shaft
          reg_dict['rSHFTL'].append(index+1)
        elif (i<ncol):
          marray[index] = mat_dict.get('S_HALITE') #Salado
          reg_dict['rS_HALITE'].append(index+1)
      elif (j < 25):
        if (i<ibh):
          marray[index] = mat_dict.get('IMPERM_Z') #Los Medanos (UNNAMED)
          reg_dict['rUNNAMED'].append(index+1)
        elif (i<iwp2):
          marray[index] = mat_dict.get('IMPERM_Z') #Borehole Plug (CONC_PLG)
          reg_dict['rCONC_PLG'].append(index+1)
          reg_dict['rUNNAMED'].append(index+1)
        elif (i<ishaft):
          marray[index] = mat_dict.get('IMPERM_Z') #Los Medanos
          reg_dict['rUNNAMED'].append(index+1)
        elif (i<iexp):
          marray[index] = mat_dict.get('CAVITY_4') #Upper Shaft (SHFTU)
          reg_dict['rSHFTU'].append(index+1)
        elif (i<ncol):
          marray[index] = mat_dict.get('IMPERM_Z') #Los Medanos
          reg_dict['rUNNAMED'].append(index+1)
      elif (j < 26):
        if (i<ibh):
          marray[index] = mat_dict.get('IMPERM_Z') #Culebra
          reg_dict['rCULEBRA'].append(index+1)
        elif (i<iwp2):
          marray[index] = mat_dict.get('IMPERM_Z') #Upper Borehole
          reg_dict['rCULEBRA'].append(index+1)
          reg_dict['rBH_OPEN'].append(index+1)
        elif (i<ishaft):
          marray[index] = mat_dict.get('IMPERM_Z') #Culebra
          reg_dict['rCULEBRA'].append(index+1)
        elif (i<iexp):
          marray[index] = mat_dict.get('CAVITY_4') #Upper Shaft
          reg_dict['rSHFTU'].append(index+1)
        elif (i<ncol):
          marray[index] = mat_dict.get('IMPERM_Z') #Culebra
          reg_dict['rCULEBRA'].append(index+1)
      elif (j < 27):
        if (i<ibh):
          marray[index] = mat_dict.get('IMPERM_Z') #Tamarisk
          reg_dict['rTAMARISK'].append(index+1)
        elif (i<iwp2):
          marray[index] = mat_dict.get('IMPERM_Z') #Upper Borehole
          reg_dict['rTAMARISK'].append(index+1)
          reg_dict['rBH_OPEN'].append(index+1)
        elif (i<ishaft):
          marray[index] = mat_dict.get('IMPERM_Z') #Tamarisk
          reg_dict['rTAMARISK'].append(index+1)
        elif (i<iexp):
          marray[index] = mat_dict.get('CAVITY_4') #Upper Shaft
          reg_dict['rSHFTU'].append(index+1)
        elif (i<ncol):
          marray[index] = mat_dict.get('IMPERM_Z')  #Tamarisk
          reg_dict['rTAMARISK'].append(index+1)
      elif (j < 28):
        if (i<ibh):
          marray[index] = mat_dict.get('IMPERM_Z') #Magenta
          reg_dict['rMAGENTA'].append(index+1)
        elif (i<iwp2):
          marray[index] = mat_dict.get('IMPERM_Z') #Upper Borehole
          reg_dict['rMAGENTA'].append(index+1)
          reg_dict['rBH_OPEN'].append(index+1)
        elif (i<ishaft):
          marray[index] = mat_dict.get('IMPERM_Z') #Magenta
          reg_dict['rMAGENTA'].append(index+1)
        elif (i<iexp):
          marray[index] = mat_dict.get('CAVITY_4') #Upper Shaft
          reg_dict['rSHFTU'].append(index+1)
        elif (i<ncol):
          marray[index] = mat_dict.get('IMPERM_Z') #Magenta
          reg_dict['rMAGENTA'].append(index+1)
      elif (j < 29):
        if (i<ibh):
          marray[index] = mat_dict.get('IMPERM_Z') #49er
          reg_dict['rFORTYNIN'].append(index+1)
        elif (i<iwp2):
          marray[index] = mat_dict.get('IMPERM_Z') #Upper Borehole
          reg_dict['rFORTYNIN'].append(index+1)
          reg_dict['rBH_OPEN'].append(index+1)
        elif (i<ishaft):
          marray[index] = mat_dict.get('IMPERM_Z') #49er
          reg_dict['rFORTYNIN'].append(index+1)
        elif (i<iexp):
          marray[index] = mat_dict.get('CAVITY_4') #Upper Shaft
          reg_dict['rSHFTU'].append(index+1)
        elif (i<ncol):
          marray[index] = mat_dict.get('IMPERM_Z') #49er
          reg_dict['rFORTYNIN'].append(index+1)
      elif (j < 31):
        if (i<ibh):
          marray[index] = mat_dict.get('IMPERM_Z') #Dewey Lake
          reg_dict['rDEWYLAKE'].append(index+1)
          if (j == 30): reg_dict['rUNSAT'].append(index+1)
        elif (i<iwp2):
          marray[index] = mat_dict.get('IMPERM_Z') #Upper Borehole
          reg_dict['rDEWYLAKE'].append(index+1)
          if (j == 30): reg_dict['rUNSAT'].append(index+1)
          reg_dict['rBH_OPEN'].append(index+1)
        elif (i<ishaft):
          marray[index] = mat_dict.get('IMPERM_Z') #Dewey Lake
          reg_dict['rDEWYLAKE'].append(index+1)
          if (j == 30): reg_dict['rUNSAT'].append(index+1)
        elif (i<iexp):
          marray[index] = mat_dict.get('CAVITY_4') #Upper Shaft
          reg_dict['rSHFTU'].append(index+1)
        elif (i<ncol):
          marray[index] = mat_dict.get('IMPERM_Z') #Dewey Lake
          reg_dict['rDEWYLAKE'].append(index+1)
          if (j == 30): reg_dict['rUNSAT'].append(index+1)
      elif (j < 33):
        if (i<ibh):
          marray[index] = mat_dict.get('IMPERM_Z') #Santa Rosa
          reg_dict['rSANTAROS'].append(index+1)
          reg_dict['rUNSAT'].append(index+1)
        elif (i<iwp2):
          marray[index] = mat_dict.get('IMPERM_Z') #Borehole Plug
          reg_dict['rSANTAROS'].append(index+1)
          reg_dict['rUNSAT'].append(index+1)
          reg_dict['rCONC_PLG'].append(index+1)
        elif (i<ishaft):
          marray[index] = mat_dict.get('IMPERM_Z') #Santa Rosa
          reg_dict['rSANTAROS'].append(index+1)
          reg_dict['rUNSAT'].append(index+1)
        elif (i<iexp):
          marray[index] = mat_dict.get('CAVITY_4') #Upper Shaft
          reg_dict['rSHFTU'].append(index+1)
        elif (i<ncol):
          marray[index] = mat_dict.get('IMPERM_Z') #Santa Rosa
          reg_dict['rSANTAROS'].append(index+1)
          reg_dict['rUNSAT'].append(index+1)
  print('Write %s' % mfilename)
  h5f = File(mfilename,'w')
  materials_group = h5f.create_group('Materials')
  h5dset = materials_group.create_dataset('Cell Ids',data=iarray)
  h5dset = materials_group.create_dataset('Material Ids', data=marray)
  h5f.close()
  print('Write region .txt files')
  for key in reg_dict:
    f = file(key+'.txt','w')
    for id in reg_dict.get(key):
      f.write('%i\n' % id)
    f.close()
  print('Write REGION blocks to %s' % rfilename)
  f = file(rfilename,'w')
  for key in reg_dict:
    reg_to_file(f,key)
  f.close()
  print('Write STRATA blocks to %s' % sfilename)
  f = file(sfilename,'w')
  sinit_to_file(f,mfilename)
  f.close()
  print('Write OBSERVATION blocks to %s' % ofilename)
  f = file(ofilename,'w')
  obs = ['rWAS_AREA','rSROR','rNROR','rSPCS','rMPCS','rNPCS','rOPS_AREA','rEXP_AREA']
  for region in obs:
    obs_to_file(f,region)
  f.close()
    
  #I don't need to write the time changes, pre_pflotran is going to do it.

  return reg_dict

def ic_to_chpt(chptname,icp_chpt,reg_dict):
  '''Write initial State and state variables to checkpoint file for use as initial conditions.
     By replacing /Checkpoint/PMCSubsurface/flow/Primary_Variables and /State in pre-existing checkpoint
     chptname = name of checkpoint.h5 file to alter
     icp_chpt = numpy array holding liquid pressure, mole fraction, temperature for each cel
     reg_dict = dictionary of regions generated in mat_wipp()
     Overwrite parts of icp_chpt, defined by regions in reg_dict, to hold two-phase initial conditions.
     Make numpy array holding State variable: 1 for liquid phase, 3 for two-phase
  '''
  state = [3 for x in range(len(icp_chpt)/2)] #force two-phase

  liquid_pressure,liquid_saturation = load('./ics.txt')

  for cellid in range(2244):
    index = cellid*2
    icp_chpt[index] = liquid_pressure[cellid]
    icp_chpt[index+1] = 1.-liquid_saturation[cellid]

  filename = 'pressure_from_ics.txt'
  f = open(filename,'w')
  for i in range(len(liquid_pressure)):
    index = i*2
    f.write('%d %f\n' % (i+1,icp_chpt[index]))
  f.close

  filename = 'saturation_from_ics.txt'
  f = open(filename,'w')
  for i in range(len(liquid_saturation)):
    index = i*2+1
    f.write('%d %f\n' % (i+1,1.-icp_chpt[index]))
  f.close

  for cellid in reg_dict['rWAS_AREA']:
    index = (cellid-1)*2
    icp_chpt[index] = REF_PRES
    icp_chpt[index+1] = gas_sat1
    #icp_chpt[index+2] = temperature
  for cellid in reg_dict['rREPOSIT']:
    index = (cellid-1)*2
    icp_chpt[index] = REF_PRES
    icp_chpt[index+1] = gas_sat1
    #icp_chpt[index+2] = temperature
  for cellid in reg_dict['rOPS_AREA']:
    index = (cellid-1)*2
    icp_chpt[index] = REF_PRES
    icp_chpt[index+1] = gas_sat1
    #icp_chpt[index+2] = temperature
  for cellid in reg_dict['rEXP_AREA']:
    index = (cellid-1)*2
    icp_chpt[index] = REF_PRES
    icp_chpt[index+1] = gas_sat1
    #icp_chpt[index+2] = temperature
  for cellid in reg_dict['rEXP_AREA']:
    index = (cellid-1)*2
    icp_chpt[index] = REF_PRES
    icp_chpt[index+1] = gas_sat1
    #icp_chpt[index+2] = temperature
  for cellid in reg_dict['rPCS']:
    index = (cellid-1)*2
    icp_chpt[index] = REF_PRES
    icp_chpt[index+1] = gas_sat1
    #icp_chpt[index+2] = temperature
  for cellid in reg_dict['rCONC_MON']:
    index = (cellid-1)*2
    icp_chpt[index] = REF_PRES
    icp_chpt[index+1] = gas_sat1
    #icp_chpt[index+2] = temperature
  for cellid in reg_dict['rSHFTL']:
    index = (cellid-1)*2
    icp_chpt[index] = REF_PRES
    icp_chpt[index+1] = gas_sat1
    #icp_chpt[index+2] = temperature
  for cellid in reg_dict['rSHFTU']:
    index = (cellid-1)*2
    icp_chpt[index] = REF_PRES
    icp_chpt[index+1] = gas_sat1 
    #icp_chpt[index+2] = temperature

  for cellid in reg_dict['rUNSAT']:
    index = (cellid-1)*2
    icp_chpt[index] = REF_PRES
# this overwrites the 3rd layer from top which should be 0.0836, not 0.08363
#geh    icp_chpt[index+1] = gas_sat4 
    #icp_chpt[index+2] = temperature
#  for cellid in reg_dict['rCULEBRA']: #single-phase liquid
#    index = (cellid-1)*2
#    icp_chpt[index] = culebra_pres
#    icp_chpt[index+1] = gassat_zero
#    #icp_chpt[index+2] = temperature
#  for cellid in reg_dict['rMAGENTA']: #single-phase liquid
#    index = (cellid-1)*2
#    icp_chpt[index] = magenta_pres
#    icp_chpt[index+1] = gassat_zero
#    #icp_chpt[index+2] = temperature
#  for cellid in reg_dict['rCASTILER']: #single-phase liquid
#    index = (cellid-1)*2
#    icp_chpt[index] = brine_pres
#    icp_chpt[index+1] = gassat_zero
#    #icp_chpt[index+2] = temperature
  filename = 'pressure_after_overwrite.txt'
  f = open(filename,'w')
  for i in range(len(liquid_pressure)):
    index = i*2
    f.write('%d %f\n' % (i+1,icp_chpt[index]))
  f.close

  filename = 'saturation_after_overwrite.txt'
  f = open(filename,'w')
  for i in range(len(liquid_saturation)):
    index = i*2+1
    f.write('%d %f\n' % (i+1,1.-icp_chpt[index]))
  f.close

  state_array = np.array(state,'=f8')
  h5 = File(chptname,'r+')
  h5['Checkpoint/PMCSubsurface/flow/Primary_Variables'][:]= icp_chpt
#  h5.create_dataset('Checkpoint/PMCSubsurface/flow/Primary_Variables', data=icp_chpt)
  h5['Checkpoint/PMCSubsurface/flow/State'][:] = state_array
  h5.close() 
  return icp_chpt

def wipp_boundaries(f,icp_chpt):
  '''pull pressures from ic array and use them as bc at top of domain and
     at sides of Culebra and Magenta.  Need also saturation at top of domain,
     which is a global variable.
     Must run this after ic_to_chpt.
     Need to know cell ids of culebra and magenta, which I do
  '''
  culebra_id = 1701
  magenta_id = 1837
  santaro_id = 2177
  index = (culebra_id-1)*2
  flow1_to_file(f,'culebra',icp_chpt[index],icp_chpt[index+1],temperature)
  index = (magenta_id-1)*2
  flow1_to_file(f,'magenta',icp_chpt[index],icp_chpt[index+1],temperature)
  index = (santaro_id-1)*2
  flow3_to_file(f,'top',icp_chpt[index],icp_chpt[index+1],temperature)
  return


if __name__ == '__main__':
  dimensions = read_dimensions()
  icp = wipp_uge(dimensions)
  reg = wipp_mat()
  planview_areas(dimensions)

  f = file(gfilename,'w')
  f.write('#choose one of these\n')
  grid_to_file(f,outfilename,adjust_gravity(dip))
  grid_to_file(f,outfile2name,adjust_gravity(0.0))
  f.close

  icp_chpt = ic_to_chpt('../checkpoint_time-5y/checkpoint_time-5y.h5',icp,reg)
  
  f = file(bcfilename,'w')
  wipp_boundaries(f,icp_chpt)
  f.close()
  
