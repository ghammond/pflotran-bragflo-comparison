import sys
import math
from h5py import *
import numpy

def load(filename):
  '''Loads liquid pressure and liquid saturation initial conditions from the
     corresponding blocks in a BRAGFLO input file and returns them as numpy
     arrays
  '''
  f = open(filename)
  nx = 68
  ny = 1
  nz = 33
  n = nx*ny*nz

  liquid_pressure = numpy.zeros((nx*ny*nz),'=f8')
  liquid_saturation = numpy.zeros((nx*ny*nz),'=f8')

  # liquid pressures
  f.readline()
  count = 0
  while True:
    if count >= n:
      break
    string = f.readline()
    words = string.split()
    for word in words:
      w = word.split('*')
      num_values = int(w[0])
      value = float(w[1])
      for i in range(num_values):
        liquid_pressure[count] = value
        count += 1
  
  # liquid saturations
  f.readline()
  count = 0
  while True:
    if count >= n:
      break
    string = f.readline()
    words = string.split()
    for word in words:
      w = word.split('*')
      num_values = int(w[0])
      value = float(w[1])
      for i in range(num_values):
        liquid_saturation[count] = value
        count += 1
      
  f.close()

  return(liquid_pressure,liquid_saturation) 
