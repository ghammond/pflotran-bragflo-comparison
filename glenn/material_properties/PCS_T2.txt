  ID 38
  POROSITY 0.03093 #0.05d0 #LHS and BRAGFLO, sampled or not?
  TORTUOSITY 1.0000000d+00
  ROCK_DENSITY 2.6500000d+03
  HEAT_CAPACITY 830.
  THERMAL_CONDUCTIVITY_WET 4.5
  THERMAL_CONDUCTIVITY_DRY 4.5
  SOIL_COMPRESSIBILITY_FUNCTION BRAGFLO
  BULK_COMPRESSIBILITY 8.0000000d-11 #bulk
  EXTERNAL_FILE ../material_properties/soil_reference_pressure.txt
  PERMEABILITY
    PERM_X_LOG10 -20.45 #-18.6d0
    PERM_Y_LOG10 -20.45 #-18.6d0
    PERM_Z_LOG10 -20.45 #-18.6d0
  /

