'''
Extracts initial pressures from checkpoint file and store them in a datset
Glenn Hammond 6/22/17
'''

import numpy as np
from h5py import *

filename = 'pflotran-0.0000y.h5'

h5file = File(filename,'r+')
primary_variables = np.array(h5file['Checkpoint/PMCSubsurface/flow/Primary_Variables'][:],'=f8')
h5file.close()

filename = 'initial_pressure_dataset.h5'
h5file = File(filename,'w')
# cell ids
iarray = np.arange(1,primary_variables.size/2+1,dtype='=i4')
h5file.create_dataset('Cell Ids',data=iarray)
# pressure array
farray = np.zeros(iarray.size,dtype='=f8')
for i in range(farray.size):
  index = i*2
  farray[i] = primary_variables[index]
h5file.create_dataset('soil_reference_pressure',data=farray)
h5file.close()

print('done')
