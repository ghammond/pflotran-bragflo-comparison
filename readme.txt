Instructions for running simulation to 10 ky (or at least to borehole intrusion).

Some particulars of the instructions are explained by the following:
PFLOTRAN does not at this time have the option to start a simulation at times less than 0 yrs.
It also does not allow resetting of pressures and saturations at will during a simulation.
And it isn't clear to me whether I could have set up initial pressures and saturations using
a cell-indexed dataset, but I didn't - I modified a checkpoint file instead.

Steps:
1. in /grid/ python wipp_uge.py. It takes as input cell_dimensions.txt and pflotran-0.0000y.h5. It modifies
pflotran-0.0000y.h5 (initial pressures and saturations), and creates a number of files:

	wipp.uge - grid file for PFLOTRAN without dipping Salado
	wipp-domain.h5 - grid w/o dip for Paraview, real dimensions
	domain-equal.h5 - grid w/o dip for Paraview, equal cell dimensions

	wipp-dip.uge - grid file for PFLOTRAN _WITH_ dipping Salado (use this one)	
	wipp-domain-dip.h5 - grid WITH dip for Paraview, real dimensions
	domain-dip-equal.h5 - grid WITH dip for Paraview, equal cell dimensions

	materials.h5 - time = -5 y materials for time-5y simulation

	bc.txt - BOUNDARY_CONDITION cards (already copied to input deck)
	grid.txt - GRID card (w and w/o dip) (already copied to input deck)
	obspts.txt - OBSERVATION cards (already copied to input deck)
	planview_areas.txt - I must have printed these for debugging - you don't need them
	regions.txt - REGION cards (already copied to input deck)
	strata.txt - STRATA card for MATERIAL ../../grid/materials.h5 (no others!) (copied to input)
	wipp_culebra.ex - CONNECTIONS for Culebra boundary condition
	wipp_magenta.ex - CONNECTIONS for Magenta boundary condition
	wipp_top.ex - CONNECTIONS for top boundary condition

	a whole bunch of region files that I am not listing, but you definitely need, e.g. rCULEBRA.txt	
	
2. in /pflotran_input/time-5y run the simulation (on one core: pflotran)

3. cp /pflotran_input/time-5y/pflotran-5.0000y.h5 /pflotran_input/restart0y/pflotran-restart0y.h5

4. in /pflotran_input/restart0y/ python restart0y.py. It takes as input pflotran-restart0y.h5 and many of
the r*.txt files in /grid/. It modifies pflotran-restart0y.py. You will need to figure out how to import
../../grid/wipp_uge.py for constants it contains. I have it in a directory that's in my PYTHON path. You
could soft link it.

restart0y/pflotran.in does not contain CREEP_CLOSURE, FRACTURE, or WIPP_SOURCE_SINK

5. if desired, in /pflotran_input/restart0y_creep/ run the simulation (on one core: pflotran). 
This has CREEP_CLOSURE, but no FRACTURE and no WIPP_SOURCE_SINK

6. in /pflotran_input/gasgen/ run the simulation (on one core: pflotran). This has CREEP_CLOSURE
FRACTURE, and WIPP_SOURCE_SINK. I have played with residual liquid and gas saturations (none of them
are zero any more), and with tolerances including ITOL_RELATIVE_UPDATE, ITOL_SCALED_RESIDUAL,
WINDOW_EPSILON, ATOL, RTOL, and STOL. Without FRACTURE or borehole intrusion, this does not need high
STOL to converge with reasonable time steps. Someday need to return zero residual saturations to zero.

7. After any pflotran run, "python write_xmf.py pflotran domain-dip-equal" to create .xmf files for
Paraview. (ln -s ../../write_xmf.py, ln -s ../../grid/domain-dip-equal.h5)
